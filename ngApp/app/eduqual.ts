export class EduQual {
    college_name: string;
    grad_year: number;
    degree:number;
    static degree_labels=["BE", "BTech", "MBA", "MS"];
}