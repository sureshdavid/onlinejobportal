
import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/toPromise';

import { EduQual } from './eduqual';
import { MOCK_EDUQUALS } from './mock-jobseeker'; 

@Injectable()
export class JobSeekerService {
        constructor(private http: Http) { }
        private handleError(error: any): Promise<any> {
            console.error('An error occurred', error); // for demo purposes only
            return Promise.reject(error.message || error);
        }
        getEduQuals(): Promise<EduQual[]> {
            return this.http.get("/jobseeker/get-edu-quals")
                       .toPromise()
                       .then(response => response.json().edu_quals as EduQual[])
                       .catch(this.handleError);
        }
        createEduQual(data): Promise<EduQual> {
            let body = JSON.stringify(data);
            let headers = new Headers({ 'Content-Type': 'application/json' });
            let options = new RequestOptions({ headers: headers });
            return this.http.post("/jobseeker/create-edu-qual", body, options)
                       .toPromise()
                       .then(response => response.json().edu_qual as EduQual)
                       .catch(this.handleError);
        }
}