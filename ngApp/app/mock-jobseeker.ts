import { EduQual } from './eduqual';
export const MOCK_EDUQUALS: EduQual[] = [
        {college_name: "IIT Kanpur", grad_year: 1997, degree:1},
        {college_name: "Rutgers University", grad_year: 1999, degree:2}
];