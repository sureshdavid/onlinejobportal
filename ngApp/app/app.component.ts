import { Component } from '@angular/core';
import { EduQual } from './eduqual';
import { JobSeekerService } from './jobseeker.service';
import { OnInit } from '@angular/core';

@Component({
    selector: 'ojp-edu-qual',
    providers: [JobSeekerService],
    template: `
            <div class='container'>
                <div class='row'>
                    <div class="col-md-8">
                        <h3>Educational Qualifications</h3>
                    </div>
                    <div class="col-md-4">
                        <button type="button" class="btn-lg btn-default pull-right" (click)="showEduQualForm()">Add</button>
                    </div>
                </div>
                <div class='row'>
                     <div class="col-md-12">
                        <form class="row hidden">
                             <div class="form-group col-md-4">
                               <label for="college">College/Institute</label>
                               <input [(ngModel)]="eduqual.college_name" type="text" class="form-control" id="college" name="college" required>
                             </div>
                             <div class="form-group col-md-4">
                               <label for="grad_year">Graduation Year</label>
                               <input [(ngModel)]="eduqual.grad_year" type="text" class="form-control" name="grad_year" id="grad_year" required>
                             </div>
                             <div class="form-group col-md-2">
                               <label for="degree">Degree</label>
                               <select [(ngModel)]="eduqual.degree" class="form-control" name="degree" id="degree">
                                    <option value="0">B.E.</option>
                                    <option value="1">B.Tech</option>
                                    <option value="2">MBA</option>
                                    <option value="3">MS</option>
                               </select>
                             </div>
                             <div class="col-md-2">
                                  <button type="button" class="btn-default pull-right" (click)="saveEduQual()">Save</button>
                                  <button type="button" class="btn-default pull-right" (click)="hideEduQualForm()">Cancel</button>
                             </div>
                        </form>
                     </div>
                </div>
                <div class='row'>
                    <div class="col-md-12">
                        <table class="table table-striped">
                            <tr>
                                <th>College/Institute</th>
                                <th>Degree</th>
                                <th>Year</th>
                            </tr>
                            <tr *ngIf="!eduquals">
                              <td colspan=3>No Educational qualifactions have been entered</td>
                            </tr>
                            <tr *ngFor="let eduqual of eduquals " >
                              <td>{{eduqual.college_name}}</td>
                              <td>{{klass.degree_labels[eduqual.degree]}}</td>
                              <td>{{eduqual.grad_year}}</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
              `
})
export class AppComponent implements OnInit { 
    eduquals: EduQual[] = [];
    eduqual: EduQual = {college_name: "", grad_year: null, degree:0};
    klass = EduQual;
    constructor(private jobSeekerService: JobSeekerService) {}
    showEduQualForm(): void {
        /*alert("hello");*/
        $("ojp-edu-qual").find("form").removeClass("hidden").addClass("show");
    }
    hideEduQualForm(): void {
        //alert("hello");
        $("ojp-edu-qual").find("form").removeClass("show").addClass("hidden");
    }
    saveEduQual(): void {
        /*alert("hello");*/
        this.jobSeekerService.createEduQual({"college_name":this.eduqual.college_name, 
                                             "grad_year":this.eduqual.grad_year,
                                             "degree":this.eduqual.degree
                                            }).then(eduqual => 
                                                    {
                                                     this.eduquals.push(eduqual);
                                                     this.eduqual={college_name: "", grad_year: null, degree:0};
                                                     this.hideEduQualForm();
                                                    });
    }
    
    ngOnInit(): void {
        //this.eduquals = this.jobSeekerService.getEduQuals();
        this.jobSeekerService.getEduQuals().then(x => this.eduquals = x);
    }
}