"""ojp URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from job_posting import views
from django.http.response import HttpResponse
from ojp import settings
from django.conf.urls.static import static

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', views.home),
    url(r'^page/(fruits\.html)',views.get_fruits_page),
    url(r'^page/(searchPage\.html)',views.get_search_results),
    url(r'^page/(.*\.html)',views.get_page),
    url(r'^random-lists$', views.random_list),
    url(r'^random-lists-json',views.random_list_json),
    url(r'^term-definition',views.term_definiton),
    url(r'^term-definition',views.get_filtered_results),
    url(r'^jobseeker-register',views.register_jobseeker),
    url(r'^recruiter-register', views.register_recruiter),
    url(r'^login', views.site_login),
    url(r'^home', views.home_page),
    url(r'create-job-posting', views.create_job_posting),
    url(r'^jobseeker-profile', views.jobseeker_profile),
    url(r'apply-job-posting', views.apply_for_job),
    url(r'get-job-applications', views.get_job_applications),
    url(r'jobseeker/get-edu-quals', views.get_edu_qualifications),
    url(r'jobseeker/create-edu-qual', views.create_edu_qualification)
    
]

urlpatterns = urlpatterns + static(settings.ANGULAR_URL, document_root=settings.ANGULAR_ROOT)

def handler404(request):
    return HttpResponse("asdfsdfsdafsafdsdafds")