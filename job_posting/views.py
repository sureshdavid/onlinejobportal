from django.shortcuts import render, redirect
from django.http.response import HttpResponse, JsonResponse
from django.template import loader
from django.conf import settings
import random
from django.db.models import Q
from job_posting.forms import JobSeekerRegistrationForm, LoginForm,\
    RecruiterRegForm, JobPostingForm
from job_posting.models import JobSeeker, Recruiter, JobPosting, JobApplication
from django.contrib.auth.models import User
from django.contrib.auth import hashers, login, authenticate
import datetime, json
from django.contrib.auth.decorators import login_required
from job_posting.models import EducationQualification
#from job_posting.models import JobPosting

# Create your views here.
def home(request):
    print request
    return HttpResponse("Hello suresh")

def register_jobseeker(request):
    if request.POST:
        jsrf = JobSeekerRegistrationForm(request.POST)
        if jsrf.is_valid():
            user = User(username=jsrf.cleaned_data['username'], 
                        first_name=jsrf.cleaned_data['first_name'],
                        last_name=jsrf.cleaned_data['last_name'],
                        email=jsrf.cleaned_data['email_address'],
                        password=hashers.make_password(jsrf.cleaned_data['password']),
                        is_staff = False,
                        is_active = True)
            user.save()
            js = JobSeeker.objects.create(mobile_phone_no=jsrf.cleaned_data['mobile_phone'],profile_status=0, user= user)
            return HttpResponse("Jobseeker account has been succesfully created")
        else:
            return HttpResponse(loader.render_to_string('jobseeker-register.html', {"form":jsrf}, request))
    else:
        jsrf = JobSeekerRegistrationForm()
        return HttpResponse(loader.render_to_string('jobseeker-register.html', {"form":jsrf}, request))

def register_recruiter(request):
    if request.POST:
        rrf = RecruiterRegForm(request.POST)
        if rrf.is_valid():
            user = User(username=rrf.cleaned_data['username'],
                        first_name=rrf.cleaned_data['first_name'],
                        last_name=rrf.cleaned_data['last_name'],
                        email=rrf.cleaned_data['email_address'],
                        password=hashers.make_password(rrf.cleaned_data['password']),
                        is_staff=False,
                        is_active=True)
            user.save()
            js = Recruiter.objects.create(mobile_phone_no=rrf.cleaned_data['mobile_phone'],
                                          company_name=rrf.cleaned_data['company_name'],
                                          user=user)
            return HttpResponse("Recruiter account has been successfully created")
        else:
            return HttpResponse(loader.render_to_string('recruiter-register.html', {"form":rrf}, request))
    else:
        rrf = RecruiterRegForm()
        return HttpResponse(loader.render_to_string('recruiter-register.html', {"form":rrf}, request))
    
# def home_page(request):
#     if request.user.is_authenticated:
#         if hasattr(request.user,'recruiter'):
#             return HttpResponse("WElcome to recruiter oage")
#         elif hasattr(request.user,'jobseeker'):
#             return HttpResponse("welcome to jobseeker")
#         else:
#             return HttpResponse("unknown user type")
#     else:
#         return redirect("/login?next=/home")

def site_login(request):
    if request.POST:
        login_form = LoginForm(request.POST)
        if login_form.is_valid():
            curr_user = authenticate(username=login_form.cleaned_data['username'], 
                                     password=login_form.cleaned_data['password'])
            if curr_user:
                login(request,curr_user)
                return redirect("/home")
            else:
                return HttpResponse("You provided incorrect credentials")
    else:
        return HttpResponse(loader.render_to_string('Login.html', {"form":LoginForm()}, request))
    
def home_page(request):
    if request.user.is_authenticated:
        if hasattr(request.user, 'recruiter'):
            jpf = JobPostingForm({"company_name":request.user.recruiter.company_name})
            job_postings = JobPosting.objects.filter(recruiter=request.user.recruiter)
            return HttpResponse(loader.render_to_string('recruiter-home.html', 
                                                        {"form":jpf, "job_postings":job_postings}, 
                                                        request))
        elif hasattr(request.user, 'jobseeker'):
            job_postings = JobPosting.objects.filter(status=1)
            job_postings_applied = JobSeeker.objects.filter(user = request.user)[0].job_postings.all()
            job_applications = dict([(ja.job_posting.id, ja) for ja in JobApplication.objects.filter(job_seeker=request.user.jobseeker)])
            return HttpResponse(loader.render_to_string('jobseeker-home.html', 
                                                        {"job_postings":job_postings,
                                                         "job_postings_applied":job_postings_applied,
                                                         "job_applications":dict(job_applications)},
                                                         request))
        else:
            return HttpResponse("Unknown user type")
    else:
        return redirect("/login?next=/home")
    
def create_job_posting(request):
    if hasattr(request.user, 'recruiter'):
        jpf = JobPostingForm(request.POST)
        if jpf.is_valid():
            jp = JobPosting.objects.create(title = jpf.cleaned_data['title'],
                                      description = jpf.cleaned_data['description'],
                                      company_name = jpf.cleaned_data['company_name'],
                                      recruiter = request.user.recruiter,
                                      status = 1,
                                      expiry_date = datetime.date.today() + datetime.timedelta(days=30),
                                      last_renewal_date = datetime.date.today() + datetime.timedelta(days=30))
            return HttpResponse(loader.render_to_string('job-posting-detail.html', {"jp":jp}))
            
    else:
        return HttpResponse("You cannot create job posting.")
    
@login_required(login_url="/login")    
def apply_for_job(request):
    if hasattr(request.user, 'jobseeker'):
        jp_id = request.POST['job_posting_id']
        JobApplication.objects.create(job_seeker = request.user.jobseeker,
                                      job_posting = JobPosting.objects.get(id=jp_id),
                                      application_date = datetime.date.today(),
                                      application_status = 1)
        return HttpResponse("Success")

# @login_required(login_url="/login")    
# def apply_for_job(request):
#     if hasattr(request.user, 'jobseeker'):
#         jp_id = request.POST['job_posting_id']
#         JobApplication.objects.create(job_seeker = request.user.jobseeker,
#                                       job_posting = JobPosting.objects.get(id=jp_id),
#                                       application_date = datetime.date.today(),
#                                       application_status = 1)
#         return HttpResponse("Success")
    
@login_required(login_url="/login")    
def get_job_applications(request):
    if hasattr(request.user, 'recruiter'):
        jp_id = request.GET['job_posting_id']
        job_applications = JobApplication.objects.filter(job_posting__id = jp_id)
        return HttpResponse(loader.render_to_string('job-applications.html', {"job_applications":job_applications}))
            
    


def get_page(request, page_name):
    template = loader.get_template(page_name)
    context = {}#'fruitslist' : ["apple", "banana", "orange", "kiwi", "mango", "pear", "palm", "papaya", "peach", "coconut"]}
    return HttpResponse(template.render(context, request))

def get_fruits_page(request, page_name):
    template = loader.get_template(page_name)
    context = {'fruitslist' : ["apple", "banana", "orange", "kiwi", "mango", "pear", "palm", "papaya", "peach", "coconut"],
               'specialFruits':["orange","mango","peach"]}
    return HttpResponse(template.render(context, request))

def get_search_results(request, page_name):
    from job_posting.models import JobPosting
    template = loader.get_template(page_name)
    context = {'fruitslist':list(JobPosting.objects.all())}
    if request.method == "GET":
        try:
            searchkey = request.GET.get("q")
            #context = {'fruitslist':list(JobPosting.objects.filter(description__icontains = searchkey, status__exact = 0))}
            context = {'fruitslist':list(JobPosting.objects.filter(Q(description__icontains = searchkey) | 
                                                                   Q(title__icontains = searchkey) |
                                                                   Q(company_name__icontains = searchkey), Q(status__exact = 1)))}
        except:
            pass
            #context = {'fruitslist':list(JobPosting.objects.filter(status__exact = 1))}
    return HttpResponse(template.render(context, request))

def get_filtered_results(request):
    from job_posting.models import JobPosting

    if request.method == "POST":
        try:
            num_fruits = int(request.POST.get("q"))
        except:
            return HttpResponse("<ul><li>Numbers only please</li></ul>")
    if request.method == "GET":
        try:
            num_fruits = int(request.GET.get("n"))
        except:
            return HttpResponse("<ul><li>Numbers only please</li></ul>")
    context = {'fruitslist':list(JobPosting.objects.filter(status__exact = 1))}
    return HttpResponse(context)

def random_list(request):
    l_randomList = []
    fruits = ["apple", "banana", "orange", "kiwi", "mango", "pear", "palm", "papaya", "peach", "coconut"]
    if request.method == "POST":
        try:
            num_fruits = int(request.POST.get("n"))
        except:
            return HttpResponse("<ul><li>Numbers only please</li></ul>")
    if request.method == "GET":
        try:
            num_fruits = int(request.GET.get("n"))
        except:
            return HttpResponse("<ul><li>Numbers only please</li></ul>")
    l_randomList = [random.choice(fruits) for i in range(0,num_fruits)]
    return HttpResponse(l_randomList)

def random_list_json(request):
    l_randomList = []
    fruits = ["apple", "banana", "orange", "kiwi", "mango", "pear", "palm", "papaya", "peach", "coconut"]
    if request.method == "POST":
        try:
            num_fruits = int(request.POST.get("n"))
        except:
            return HttpResponse("<ul><li>Numbers only please</li></ul>")
    if request.method == "GET":
        try:
            num_fruits = int(request.GET.get("n"))
        except:
            return HttpResponse("<ul><li>Numbers only please</li></ul>")
    l_randomList = [random.choice(fruits) for i in range(0,num_fruits)]
    return JsonResponse({"result":l_randomList})

def term_definiton(request):
    x = {'V8':'Name of latest Chrome JS Engine', 'event-driven':'Event Driven Architecture for Node JS'}
    if request.method == "POST":
        return JsonResponse({"result":x.get(request.POST.get('q','None'),'sorry no definition')})
    if request.method == "GET":
        return JsonResponse({"result":x.get(request.GET.get('q','None'),'sorry no definition')})
    
@login_required(login_url="/login")            
def jobseeker_profile(request):
    if hasattr(request.user, 'jobseeker'):
        return HttpResponse(loader.render_to_string('jobseeker-profile.html', {"ANGULAR_URL":settings.ANGULAR_URL}))
    else:
        return HttpResponse("You are not logged in as a jobseeker")

@login_required(login_url="/login")     
def get_edu_qualifications(request):
    if hasattr(request.user, 'jobseeker'):
        print "asdfsdfds"
        edu_quals = EducationQualification.objects.filter(job_seeker=request.user.jobseeker)
        resp = []
        for edu_qual in edu_quals:
            resp.append({"college_name":edu_qual.institute_name,
                         "grad_year":edu_qual.graduation_year,
                         "degree":edu_qual.degree_name})
        return JsonResponse(data={"edu_quals":resp})
    else:
        print "JPJPJP"
        return JsonResponse(data={"edu_quals":[]})

@login_required(login_url="/login")     
def create_edu_qualification(request):
    if hasattr(request.user, 'jobseeker'):
        req_data = json.loads(request.body)
        edu_qual = EducationQualification.objects.create(job_seeker=request.user.jobseeker,
                                                           degree_name = req_data["degree"],
                                                           institute_name = req_data["college_name"],
                                                           graduation_year = req_data["grad_year"])
        return JsonResponse(data={"edu_qual":{"college_name":edu_qual.institute_name,
                                             "degree":edu_qual.degree_name,
                                             "grad_year":edu_qual.graduation_year}
                                  })