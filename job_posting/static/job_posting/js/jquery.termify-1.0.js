(function($){
	function render_tooltip(responseBody){
			var span_pos = $(this).offset();
			$("div#term-def-box").css("position","absolute");
			$("div#term-def-box").css("top","0px");
			$("div#term-def-box").css("left","0px");
			$("div#term-def-box").offset({left:span_pos.left, top:span_pos.top + 20});
			$("div#term-def-box").html(responseBody['result']);
			$("div#term-def-box").show();
	}

	$.fn.termify = function(url, style) {
		$("body").append("<div id='term-def-box'></div>");		
		$("div#term-def-box").css(style);
		var termList = this.find("span.term");
		termList.mouseover(function(evt) {
			if (evt.target == this) {
				$.ajax({'url':url, type:'GET', data:{q:$(this).html()}, dataType:"json", success:render_tooltip, context:this})	
			}
		});
		termList.mouseout(function(evt) {		
			$("div#term-def-box").hide();
		});
		
		
	};
})(jQuery);
