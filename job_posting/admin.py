from django.contrib import admin

# Register your models here.
from .models import JobPosting


class JobPostingAdmin(admin.ModelAdmin):
    list_display = ("title", "company_name", "status")
    fields = ("company_name", "title", "status","description")


admin.site.register(JobPosting,JobPostingAdmin)