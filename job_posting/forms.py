from django import forms


class UserRegistrationForm(forms.Form):
    username = forms.CharField(label="Username", max_length=50, 
                               widget=forms.TextInput(attrs={"placeholder":"Username",
                                                             "class":"form-control"}))
    password = forms.CharField(label="Password",
                                   widget=forms.PasswordInput(attrs={"placeholder":"Password",
                                                             "class":"form-control"}))
    first_name = forms.CharField(label="First Name", max_length=200,
                                 widget=forms.TextInput(attrs={"placeholder":"Rakesh",
                                                             "class":"form-control"}))
    last_name = forms.CharField(label="Last Name", max_length=200,
                                widget=forms.TextInput(attrs={"placeholder":"Kumar",
                                                             "class":"form-control"}))
    email_address = forms.EmailField(label="Email Address",
                                     widget=forms.TextInput(attrs={"placeholder":"You email",
                                                             "class":"form-control"}))
    mobile_phone = forms.RegexField(label="Mobile Phone", regex=r"^\d{10}$",
                                    widget=forms.TextInput(attrs={"placeholder":"10 digit mobile number",
                                                             "class":"form-control"}),
                                    error_messages={'invalid':'Enter a valid ten digit mobile number, no spaces or dashes'})
    
class JobSeekerRegistrationForm(UserRegistrationForm):
    resume_file = forms.FileField(label="Upload Resume", required=False)
    
class RecruiterRegForm(UserRegistrationForm):
    company_name = forms.CharField(label="Company Name", max_length=100,
                                   widget=forms.TextInput(attrs={"placeholder":"Company Name",
                                                             "class":"form-control"}))
    
    
class LoginForm(forms.Form):
    username = forms.CharField(label="Username", max_length = 50,
                               widget=forms.TextInput(attrs={"placeholder":"username",
                                                      "class":"form-control"}))
    password = forms.CharField(label="Password",
                               widget=forms.PasswordInput(attrs={"placeholder":"password",
                                                                 "class":"form-=control"}))
    
class JobPostingForm(forms.Form):
    title = forms.CharField(label="Job Title", max_length=200,
                            widget=forms.TextInput(attrs={"placeholder":"Job Title",
                                                             "class":"form-control"}))
    description = forms.CharField(label="Job Description",
                                  widget=forms.Textarea(attrs={"placeholder":"Job Description",
                                                             "class":"form-control"}))
    company_name = forms.CharField(label="Company Name",
                                   widget=forms.TextInput(attrs={"placeholder":"Company Name",
                                                             "class":"form-control"}))
