from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User
from django.http.response import HttpResponse

# Create your models here.

class JobPosting(models.Model):
    JOB_STATUS = ((0,'Draft'),(1,'Published'))
    POSTING_TYPES = ((0,'Regular'),(1,'Premium'))
    title = models.CharField(max_length=256)
    description = models.TextField(max_length=2000)
    status = models.SmallIntegerField(choices=JOB_STATUS, default=0)
    company_name = models.CharField(max_length=100)
    recruiter = models.ForeignKey('Recruiter',related_name='job_posting')
    expiry_date = models.DateField()
    last_renewal_date = models.DateField()
    applicants = models.ManyToManyField('JobSeeker',through='JobApplication')
    
    def __str__(self):
        return self.title 
    
    def get_status(self):
        for st in self.JOB_STATUS:
            if st[0] == self.status:
                return st[1]
            
class Recruiter(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    mobile_phone_no = models.TextField(max_length=10)
    company_name = models.CharField(max_length=100)
    
class JobSeeker(models.Model):
    PROFILE_STATUSES = ((0,'Initial'),(1,'Active'),(2,'Inactive'),(3,'Suspended'))
    user = models.OneToOneField(User,on_delete=models.CASCADE)
    mobile_phone_no = models.TextField(max_length=10)
    resune_cv = models.FileField(upload_to="/uploads",max_length = 256)
    profile_status = models.SmallIntegerField(choices=PROFILE_STATUSES)
    job_postings = models.ManyToManyField(JobPosting,through='JobApplication')
    
class EducationQualification(models.Model):
    DEGREE_CHOICES = ((0,'B.E'),(1,'B.TEch'),(2,'MBA'),(3,'MS'))
    degree_name = models.SmallIntegerField(choices=DEGREE_CHOICES, default=0)
    institute_name = models.CharField(max_length=256)
    graduation_year = models.IntegerField()
    notes = models.TextField(max_length=500)
    job_seeker = models.ForeignKey(JobSeeker,related_name='edu_qual')
        
class WorkExeperience(models.Model):
    company_name  = models.CharField(max_length=100)
    start_date = models.DateField()
    end_date = models.DateField(blank=True)
    job_title = models.CharField(max_length=256)
    job_seeker = models.ForeignKey(JobSeeker, related_name = "work_exp")
        
class JobApplication(models.Model):
    STATUSES = ((0,'Initial'),(1,'Under Review'),(2,'Call Letter Issued'), (3,'Hired'),(4,'Rejected'))
    job_seeker = models.ForeignKey(JobSeeker)
    job_posting = models.ForeignKey(JobPosting)
    application_date = models.DateField()
    application_status = models.SmallIntegerField(choices = STATUSES)
    
    @property
    def status(self):
        for st in JobApplication.STATUSES:
            if st[0] == self.application_status:
                return st[1]
    


